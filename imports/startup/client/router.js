import React from 'react';
import { mount } from 'react-mounter';
import WithProvider from '../../ui/containers/with-provider';
import Start from '../../ui/pages/start';
import Rating from '../../ui/pages/rating';
import Dashboard from '../../ui/pages/dashboard';
import layoutFlex from '../../ui/HOC/LayoutFlex';

FlowRouter.route('/', {
  action: () => mount(WithProvider, {
    content: () => (<Start />)
  }),
  name: 'start'
});

FlowRouter.route('/rating', {
  action: () => mount(layoutFlex(WithProvider), {
    content: () => (<Rating />)
  }),
  name: 'rating'
});

FlowRouter.route('/dashboard', {
  action: () => mount(WithProvider, {
    content: () => (<Dashboard />)
  }),
  name: 'dashboard'
});
