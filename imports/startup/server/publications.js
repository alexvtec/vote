import { Meteor } from 'meteor/meteor';
import Ratings from '../../../lib/ratings';

Meteor.publish('ratings', () => Ratings.find({}));
