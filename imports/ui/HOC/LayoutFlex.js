import React from 'react';

import styles from './layout-flex-styles.css';

const LayoutFlex = Component => (props) => {
  return (
    <div className={styles.flex}>
      <Component {...props} />
    </div>
  );
};

export default LayoutFlex;
