import types from '../action-types';

const updateRatings = (ratings) => ({ type: types.UPDATE_RATINGS, ratings });

const placeVote = dispatch => rating => Meteor.call('putVote', rating, (err, res) => {
  if (err) {
    dispatch({
      type: types.PLACE_VOTE_ERROR
    });
  } else {
    dispatch({
      type: types.PLACE_VOTE_SUCCESS
    });
  }
});

export default {
  updateRatings,
  placeVote
};
