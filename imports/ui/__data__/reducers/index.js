import _ from 'lodash';
import types from '../action-types';

const INITIAL_STATE = {
  isSubmitted: false,
  error: null,
  ratings: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.PLACE_VOTE_SUCCESS:
      return _.assign({}, state, { isSubmitted: true });
    case types.PLACE_VOTE_ERROR:
      return _.assign({}, state, { isSubmitted: false, error: action.error });
    case types.UPDATE_RATINGS:
      return _.assign({}, state, { ratings: action.ratings });
    default:
      return state;
  }
}
