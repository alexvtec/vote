import { createSelector } from 'reselect';
import _ from 'lodash';

const mappings = {
  AWFUL: 1,
  BAD: 2,
  NEUTRAL: 3,
  GOOD: 4,
  AMAZING: 5
};

const HUNDRED = 100;
const ZERO = 0;

const ratingItemsSelector = state => state.ratings;
const isSubmittedSelector = state => state.isSubmitted;

const getVotesNumber = createSelector(
  [ratingItemsSelector],
  votes => votes.length
);

const getAverageVote = createSelector(
  [ratingItemsSelector, getVotesNumber],
  (ratingItems, votesNumber) => {
    if (votesNumber <= 0) {
      return -1;
    }
    let value = 0;
    _.forEach(ratingItems, (item) => {
      value += mappings[item.value];
    });

    return value / votesNumber;
  }
);

const getMappedMoods = createSelector(
  [getAverageVote],
  (average) => {
    const moods = _.keys(mappings);
    const dividedIndex = _.floor(average);

    return _.map(moods, (mood, index) => {
      let value = ZERO;
      if (index < dividedIndex) {
        value = HUNDRED;
      } else if (index === dividedIndex) {
        value = _.round(((average - dividedIndex) * HUNDRED), 2);
      }

      return { title: mood, value };
    });
  }
);

const percentageSelector = createSelector(
  [ratingItemsSelector, getVotesNumber],
  (items, votesNumber) => {
    if (votesNumber === 0) {
      return -1;
    }
    const map = {};
    let total = 0;
    _.forEach(items, (item) => {
      map[item.value] = map[item.value] ? (map[item.value] + 1) : 1;
      total += mappings[item.value];
    });
    const criteria = 'AMAZING';
    const max = mappings[criteria] * votesNumber;
    return _.round((total / max) * HUNDRED, 2);
  }
);

const levelSelector = createSelector(
  [percentageSelector],
  (percentage) => {
    let level = null;

    if (percentage === -1) {
      level = 'GRAY';
    } else if (_.inRange(percentage, 0, 39.9)) {
      level = 'RED';
    } else if (_.inRange(percentage, 40, 79.9)) {
      level = 'YELLOW';
    } else {
      level = 'GREEN';
    }

    return {
      level,
      percentage
    };
  }
);

export default {
  getAverageVote,
  percentageSelector,
  isSubmittedSelector,
  levelSelector,
  getMappedMoods
};
