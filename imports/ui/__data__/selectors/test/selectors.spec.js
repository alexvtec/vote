/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import { chai } from 'meteor/practicalmeteor:chai';
import selectors from '../index';

describe('SELECTORS:', function() {
  describe('percentage', function() {
    it('returns -1 for no entries', function() {
      const state = { ratings: [] };
      const expected = -1;
      const actual = selectors.percentageSelector(state);
      chai.assert.equal(actual, expected, 'returns -1 for no entries');
    });
    it('returns 100 for 1 entry', function() {
      const state = { ratings: [{ value: 'one' }] };
      const expected = 100;
      const actual = selectors.percentageSelector(state);
      chai.assert.equal(actual, expected, 'returns 100% for one entry');
    });
    it('returns 100 for 2 equal entries', function() {
      const state = { ratings: [{ value: 'one' }, { value: 'one' }] };
      const expected = 100;
      const actual = selectors.percentageSelector(state);
      chai.assert.equal(actual, expected, 'should return 100% for two equal entries');
    });
    it('returns 50 for 2 different entries', function() {
      const state = { ratings: [{ value: 'one' }, { value: 'two' }] };
      const expected = 50;
      const actual = selectors.percentageSelector(state);
      chai.assert.equal(actual, expected, 'should return 50% for two different entries');
    });
    it('returns 20 for 5 different entries', function() {
      const state = {
        ratings: [
          { value: 'one' }, { value: 'two' },
          { value: 'three' }, { value: 'four' },
          { value: 'five' }
        ]
      };
      const expected = 20;
      const actual = selectors.percentageSelector(state);
      chai.assert.equal(actual, expected, 'should return 20% for 5 different entries');
    });
  });
  describe('average vote returns', function() {
    it('-1 for no entries', function() {
      const state = { ratings: [] };
      const expected = -1;
      const actual = selectors.getAverageVote(state);
      chai.assert.equal(actual, expected, 'should return -1 for no entries');
    });
    it('average value for 1 entry', function() {
      const state = { ratings: [{ value: 'AMAZING' }] };
      const expected = 5;
      const actual = selectors.getAverageVote(state);
      chai.assert.equal(actual, expected, `should return average value of ${expected}`);
    });
    it('average value for 2 entries', function() {
      const state = {
        ratings: [
          { value: 'AMAZING' },
          { value: 'GOOD' }
        ]
      };
      const expected = 4.5;
      const actual = selectors.getAverageVote(state);
      chai.assert.equal(actual, expected, `should return average value of ${expected}`);
    });
    it('average for 10 entries', function() {
      const state = {
        ratings: [
          { value: 'BAD' },
          { value: 'GOOD' },
          { value: 'AMAZING' },
          { value: 'AMAZING' },
          { value: 'AWFUL' },
          { value: 'AMAZING' },
          { value: 'NEUTRAL' },
          { value: 'BAD' },
          { value: 'BAD' },
          { value: 'GOOD' }
        ]
      };
      const expected = 3.3;
      const actual = selectors.getAverageVote(state);
      chai.assert.equal(actual, expected, `should return average value of ${expected}`);
    });
  });
  describe('getMappedMoods', function() {
    it('calculate mood value for one AMAZING item', function() {
      const expected = [
        { title: 'AWFUL', value: 100 },
        { title: 'BAD', value: 100 },
        { title: 'NEUTRAL', value: 100 },
        { title: 'GOOD', value: 100 },
        { title: 'AMAZING', value: 100 },
      ];
      const state = { ratings: [{ value: 'AMAZING' }] };
      const actual = selectors.getMappedMoods(state);
      chai.assert.deepEqual(actual, expected, 'calculated value for one item');
    });
    it('calculate mood value for 5 items and percentage', function() {
      const expected = [
        { title: 'AWFUL', value: 100 },
        { title: 'BAD', value: 100 },
        { title: 'NEUTRAL', value: 100 },
        { title: 'GOOD', value: 0 },
        { title: 'AMAZING', value: 0 },
      ];

      const state = {
        ratings: [
          { value: 'AWFUL' },
          { value: 'BAD' },
          { value: 'NEUTRAL' },
          { value: 'GOOD' },
          { value: 'AMAZING' },
        ]
      };

      const actual = selectors.getMappedMoods(state);
      chai.assert.deepEqual(actual, expected, 'calculated value for 5 items');
    });
    it('calculate mood value for 5 items and average 2', function() {
      const expected = [
        { title: 'AWFUL', value: 100 },
        { title: 'BAD', value: 100 },
        { title: 'NEUTRAL', value: 0 },
        { title: 'GOOD', value: 0 },
        { title: 'AMAZING', value: 0 },
      ];

      const state = {
        ratings: [
          { value: 'AWFUL' },
          { value: 'AWFUL' },
          { value: 'AWFUL' },
          { value: 'BAD' },
          { value: 'AMAZING' }
        ]
      };
      const actual = selectors.getMappedMoods(state);
      chai.assert.deepEqual(actual, expected, 'calculated value for 5 items');
    });
    it('calculate float mood', function() {
      const expected = [
        { title: 'AWFUL', value: 100 },
        { title: 'BAD', value: 100 },
        { title: 'NEUTRAL', value: 28.57 },
        { title: 'GOOD', value: 0 },
        { title: 'AMAZING', value: 0 },
      ];

      const state = {
        ratings: [
          { value: 'AWFUL' },
          { value: 'AWFUL' },
          { value: 'AWFUL' },
          { value: 'BAD' },
          { value: 'BAD' },
          { value: 'GOOD' },
          { value: 'AMAZING' }
        ]
      };
      const actual = selectors.getMappedMoods(state);
      chai.assert.deepEqual(actual, expected, 'calculated floating value');
    });
  });
});
