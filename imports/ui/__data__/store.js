import { createStore, applyMiddleware, compose } from 'redux';
import { Tracker } from 'meteor/tracker';
import { Meteor } from 'meteor/meteor';
import ReduxThunk from 'redux-thunk';
import ratingsReducer from '../__data__/reducers/index';

import Ratings from '../../../lib/ratings';
import actions from './actions';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  ratingsReducer,
  {},
  composeEnhancers(applyMiddleware(ReduxThunk))
);

Tracker.autorun(() => {
  Meteor.subscribe('ratings');
  store.dispatch(actions.updateRatings(Ratings.find().fetch()));
});

export default store;
