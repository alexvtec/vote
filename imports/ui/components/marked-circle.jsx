import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './marked-circle-styles.css';

const MarkedCircle = props => (
  <div className={classnames(props.className, styles.checkedCircle)}>
    <span className={styles.mark}>&#x2714;</span>
  </div>
);

MarkedCircle.propTypes = {
  className: PropTypes.string
};

MarkedCircle.defaultProps = {
  className: ''
};

export default MarkedCircle;
