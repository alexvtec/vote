import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Smile from './smile';

import styles from './satisfaction-block-styles.css';

const SatisfactionBlock = (props) => {
  const { value, level } = props;
  return (
    <div className={styles.satisfaction}>
      <div className={styles.satisfaction_rating}>
        <div>
          <div>
            <span className={classnames(styles.satisfaction_rating_percentage, styles[level])}>
              { value }
            </span>
            <span className={styles.satisfaction_rating_percentSign}>&#37;</span>
          </div>
          <div className={styles.satisfaction_rating_title}>customer satisfaction rating</div>
        </div>
      </div>
      <div className={styles.satisfaction_smile}>
        <Smile mood={level} />
      </div>
    </div>
  );
};

SatisfactionBlock.propTypes = {
  value: PropTypes.number.isRequired,
  level: PropTypes.oneOf(['GREEN', 'RED', 'YELLOW'])
};

SatisfactionBlock.defaultProps = {
  level: 'GREEN'
};

export default SatisfactionBlock;
