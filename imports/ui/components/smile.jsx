import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './smile-styles.css';

const Smile = (props) => {
  return (
    <div
      className={classnames(
        styles.smile,
        styles[props.mood]
      )}
    >
      <div className={classnames(styles.mouth, `${styles.mouth}_${props.mood}`)} />
    </div>
  );
};

Smile.propTypes = {
  mood: PropTypes.oneOf(['GREEN', 'RED', 'YELLOW'])
};

Smile.defaultProps = {
  mood: 'GREEN'
};

export default Smile;
