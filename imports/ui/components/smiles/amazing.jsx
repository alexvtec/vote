import React from 'react';
import PropTypes from 'prop-types';

const Amazing = (props) => {
  return (
    <div>
      <svg width="52px" height="51px" viewBox="0 0 52 51" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="[Customer]-Rating-Page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Rating-Page" transform="translate(-846.000000, -500.000000)" stroke="#4ABED5" stroke-width="2">
            <g id="Rating" transform="translate(538.000000, 501.000000)">
              <path d="M319.119203,25.5208333 C319.389172,34.0233627 326.379977,40.8333333 334.964387,40.8333333 C343.548798,40.8333333 350.539603,34.0233627 350.809572,25.5208333 L319.119203,25.5208333 Z M334.452991,49 C348.00993,49 359,38.0309764 359,24.5 C359,10.9690236 348.00993,0 334.452991,0 C320.896053,0 309.905983,10.9690236 309.905983,24.5 C309.905983,38.0309764 320.896053,49 334.452991,49 Z" id="5"></path>
            </g>
          </g>
        </g>
      </svg>
    </div>
  );
};

Amazing.propTypes = {};

Amazing.defaultProps = {};

export default Amazing;
