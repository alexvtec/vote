import React from 'react';
import PropTypes from 'prop-types';

const Awful = (props) => {
  return (
    <div>
      <svg width="52px" height="51px" viewBox="0 0 52 51" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="[Customer]-Rating-Page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Rating-Page" transform="translate(-537.000000, -500.000000)" stroke="#4ABED5" stroke-width="2">
            <g id="Rating" transform="translate(538.000000, 501.000000)">
              <path d="M10.2638857,36.75 C10.7883188,29.3333645 16.9826913,23.4791667 24.5470085,23.4791667 C32.1113258,23.4791667 38.3056983,29.3333645 38.8301314,36.75 L10.2638857,36.75 Z M24.5470085,49 C38.103947,49 49.0940171,38.0309764 49.0940171,24.5 C49.0940171,10.9690236 38.103947,0 24.5470085,0 C10.9900701,0 0,10.9690236 0,24.5 C0,38.0309764 10.9900701,49 24.5470085,49 Z" id="1"></path>
            </g>
          </g>
        </g>
      </svg>
    </div>
  );
};

Awful.propTypes = {};

Awful.defaultProps = {};

export default Awful;
