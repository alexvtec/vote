import React from 'react';
import PropTypes from 'prop-types';

const Good = (props) => {
  return (
    <div>
      <svg width="52px" height="51px" viewBox="0 0 52 51" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="[Customer]-Rating-Page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Rating-Page" transform="translate(-769.000000, -500.000000)" stroke="#4ABED5" stroke-width="2">
            <g id="Rating" transform="translate(538.000000, 501.000000)">
              <g id="4" transform="translate(232.173789, 0.000000)">
                <ellipse id="Oval-1-Copy" cx="24.5470085" cy="24.5" rx="24.5470085" ry="24.5"></ellipse>
                <path d="M9.00419401,29.6041667 C9.00419401,29.6041667 12.2483504,41.1215779 24.0356125,41.1215779 C35.8228747,41.1215779 39.8888889,29.6041667 39.8888889,29.6041667" id="Line" stroke-linecap="square"></path>
              </g>
            </g>
          </g>
        </g>
      </svg>
    </div>
  );
};

Good.propTypes = {};

Good.defaultProps = {};

export default Good;
