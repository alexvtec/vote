import amazing from './amazing';
import good from './good';
import neutral from './neutral';
import bad from './bad';
import awful from './awful';

export default smiles = {
  amazing,
  good,
  neutral,
  bad,
  awful
};
