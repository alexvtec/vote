import React from 'react';
import PropTypes from 'prop-types';

const Neutral = (props) => {
  return (
    <div>
      <svg width="52px" height="51px" viewBox="0 0 52 51" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="[Customer]-Rating-Page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Rating-Page" transform="translate(-691.000000, -500.000000)" stroke="#4ABED5" stroke-width="2">
            <g id="Rating" transform="translate(538.000000, 501.000000)">
              <g id="3" transform="translate(154.441595, 0.000000)">
                <ellipse id="Oval-1-Copy" cx="24.5470085" cy="24.5" rx="24.5470085" ry="24.5"></ellipse>
                <path d="M13.2910622,33.1770833 L35.8035303,33.1770833" id="Line" stroke-linecap="square"></path>
              </g>
            </g>
          </g>
        </g>
      </svg>
    </div>
  );
};

Neutral.propTypes = {};

Neutral.defaultProps = {};

export default Neutral;
