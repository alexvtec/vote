import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import _ from 'lodash';
import MarkedCircle from './marked-circle';
import smiles from './smiles';

import styles from './vote-option-styles.css';

const VoteOption = (props) => {
  const { checked, disabled, filled, fillFactor, level } = props;
  const additionalStyles = {
    backgroundPositionX: `-${fillFactor}%`
  };
  const Smile = smiles[level.toLowerCase()];
  return (
    <button
      style={additionalStyles}
      type="button"
      className={classnames(styles.vote, { [styles.vote_filled]: filled })}
      onClick={props.onClick}
      name={props.level}
      disabled={disabled}
    >
      <Smile />
      { checked ? <MarkedCircle className={styles.circlePosition} /> : null }
    </button>
  );
};

VoteOption.propTypes = {
  level: PropTypes.oneOf(['AWFUL', 'BAD', 'NEUTRAL', 'GOOD', 'AMAZING']),
  onClick: PropTypes.func.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  filled: PropTypes.bool,
  fillFactor: PropTypes.number
};

VoteOption.defaultProps = {
  level: 'NEUTRAL',
  onClick: _.noop,
  checked: true,
  disabled: false,
  filled: false,
  fillFactor: 0
};

export default VoteOption;
