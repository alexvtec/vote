const moods = {
  AWFUL: 'AWFUL',
  BAD: 'BAD',
  NEUTRAL: 'NEUTRAL',
  GOOD: 'GOOD',
  AMAZING: 'AMAZING'
};

const HowDidWeDO = 'How did we do? Please rate your experience.';
const WeAreAlways = 'We’re always looking for ways to improve our customer experience.';
const ThankYou = 'Thank you.';

const sentences = {
  HowDidWeDO,
  WeAreAlways,
  ThankYou
};

export default {
  moods,
  sentences
};
