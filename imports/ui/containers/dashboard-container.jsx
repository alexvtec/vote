import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import selectors from '../__data__/selectors';
import SatisfactionBlock from '../components/satisfaction-block';

import styles from './dashboard-container-styles.css';

const mapStateToProps = state => ({ rating: selectors.levelSelector(state) });

const DashboardContainer = (props) => {
  const { rating: { level, percentage } } = props;
  if (!level || !percentage) {
    return (<div>Loading</div>);
  }
  const renderEmptyVotesBlock = () => (
    <div className={styles.centralBlock}>
      No votes yet
    </div>
  );
  const renderSatisfactionBlock = () => (
    <SatisfactionBlock value={percentage} level={level} />
  );
  return (
    <div className={styles.dashboard}>
      <div className={classnames(styles.ribbon, styles[level])} />
      <div>
        { percentage === -1 ? renderEmptyVotesBlock() : renderSatisfactionBlock() }
      </div>
      <div className={classnames(styles.ribbon, styles[level])} />
    </div>
  );
};

DashboardContainer.propTypes = {
  rating: PropTypes.shape({
    level: PropTypes.oneOf([ 'RED', 'YELLOW', 'GREEN', 'GRAY']),
    percentage: PropTypes.number
  })
};

DashboardContainer.defaultProps = {
  rating: {}
};

export default connect(mapStateToProps, null)(DashboardContainer);
