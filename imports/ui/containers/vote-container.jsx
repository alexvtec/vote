import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import VoteOption from '../components/vote-option';
import constants from '../constants';
import actions from '../__data__/actions';
import selectors from '../__data__/selectors';

import styles from './vote-container-styles.css';

const mapStateToProps = state => ({
  isSubmitted: selectors.isSubmittedSelector(state),
  moods: selectors.getMappedMoods(state)
});
const mapDispatchToProps = dispatch => ({ placeVote: dispatch(actions.placeVote) });

class VoteContainer extends PureComponent {
  constructor() {
    super();
    this.state = {
      isSubmitted: false,
      checked: null
    };
  }

  static propTypes = {
    placeVote: PropTypes.func,
    moods: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.number
    }))
  };

  static defaultProps = {
    placeVote: _.noop,
    moods: []
  };
  handleClick = (event, mood) => {
    if (this.state.isSubmitted) {
      return undefined;
    }
    this.setState({
      isSubmitted: true,
      checked: mood
    }, () => this.props.placeVote(mood));
  };

  renderMainBlock = () => {
    return (
      <div>
        <div className={styles.title}>{constants.sentences.HowDidWeDO}</div>
        <div className={styles.description}>{constants.sentences.WeAreAlways}</div>
      </div>
    );
  }

  renderConfirmation = () => {
    return (
      <div className={styles.title}>{constants.sentences.ThankYou}</div>
    );
  }

  render() {
    const { checked, isSubmitted } = this.state;
    const { moods } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.paper}>
          <div>
            <CSSTransitionGroup
              transitionName="status"
              transitionEnterTimeout={500}
              transitionLeave={false}
            >
              { !isSubmitted ? this.renderMainBlock() : null }
              { isSubmitted ? this.renderConfirmation() : null }
            </CSSTransitionGroup>
          </div>
          <div className={styles.separator} />
          <div>
            {_.map(moods, (mood, index) => (
              <VoteOption
                key={mood.title}
                level={mood.title}
                onClick={event => this.handleClick(event, mood.title)}
                checked={checked === mood.title}
                disabled={isSubmitted}
                filled={isSubmitted && mood.value === 100}
                delay={index}
                fillFactor={isSubmitted ? mood.value : 0 }
              />
            ))}
          </div>
        </div>
        <div className={styles.paper_shadow} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VoteContainer);
