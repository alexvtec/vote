import React from 'react';
import { Provider } from 'react-redux';
import store from '../__data__/store';

const withProvider = ({ content }) => (
  <Provider store={store}>
    {content()}
  </Provider>
);

module.exports = withProvider;
