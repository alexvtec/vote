import React from 'react';
import DashBoardContainer from '../containers/dashboard-container';

const Dashboard = () => (<DashBoardContainer />);

Dashboard.propTypes = {};

Dashboard.defaultProps = {};

export default Dashboard;
