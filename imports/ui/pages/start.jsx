import React from 'react';
import _ from 'lodash';

import styles from './start_styles.css';

const Start = () => {
  const routes = [
    {
      url: '/rating',
      title: 'Rating'
    },
    {
      url: '/dashboard',
      title: 'Dashboard'
    }
  ];

  return (
    <nav className={styles.menu}>
      <ul>
        {_.map(routes, route => (
          <li key={route.title}>
            <a href={route.url} className={styles.link}>
              {route.title}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

Start.propTypes = {};

Start.defaultProps = {};

export default Start;
