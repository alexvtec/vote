import { Mongo } from 'meteor/mongo';

const Ratings = new Mongo.Collection('ratings');

export default Ratings;
