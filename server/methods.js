import { Meteor } from 'meteor/meteor';
import Ratings from '../lib/ratings';

Meteor.methods({
  putVote: value => Ratings.insert({ value })
});
